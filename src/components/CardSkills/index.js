import React, {Component} from 'react'

export default class Skills extends Component {
   
    render() {
        return (
           
                <div>
                    <div className="uk-card uk-card-hover uk-card-body">
                        <h3 className="uk-card-title">{this.props.title}</h3>
                        <div uk-grid="true">
                        {this.props.children}
                        </div>
                       
                    </div>
                </div>
           

        );
    }

}