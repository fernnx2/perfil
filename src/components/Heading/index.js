import React, { Component } from 'react'

export default class Heading extends Component {
    constructor(props) { 
        super(props)
    }
  render() {
    return (
      <div>
            <h1 className="uk-heading-bullet uk-heading-line"><span>
            {this.props.title}
            </span></h1>
      </div>
    )
  }
}
