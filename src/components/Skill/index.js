import React, {Component} from 'react'

import "./Skill.css";
export default class Skill extends Component {
    constructor(props) {
        super(props);
        this.porciento = {
            "progress": "pie-wrapper progress-"
        }
    }

    render() {
        return (
           
            <div>
            <div className="set-size">
                    <label className="uk-text-large">{this.props.title}</label>

                    <div className={this.porciento.progress + this.props.porcentaje}>
                        <span className="label">{this.props.porcentaje}
                            <span className="smaller">%</span>
                        </span>
                        <div className="pie">
                            <div className="left-side half-circle"></div>
                            <div className="right-side half-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
              

        );
    }
}