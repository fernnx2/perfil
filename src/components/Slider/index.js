import React, { Component } from 'react'
import {Link} from 'react-router-dom';

export default class Slider extends Component {
    render(){
        return(
           
     <div className="uk-position-relative uk-visible-toggle uk-light"  uk-slider="autoplay:true;autoplay-interval:3000; clsActivated: uk-transition-active; center: true">

    <ul className="uk-slider-items uk-grid">
    <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/perfil.jpg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">Fernando Menjivar</h3>
                    <p className="uk-margin-remove">Web Developer & Web Designer</p>
                </div>
            </div>
        </li>
        <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/stage.jpeg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">Developer</h3>
                    <p className="uk-margin-remove">Developer of web applications mobile full stack</p>
                </div>
            </div>
        </li>
        <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/equipo.jpeg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">Teamwork</h3>
                </div>
            </div>
        </li>
        <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/front.jpg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">New technologies</h3>
                   
                </div>
            </div>
        </li>
        <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/bussines.jpg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">Business Intellisense</h3>
                
                </div>
            </div>
        </li>
        <li className="uk-width-2-5">
            <div className="uk-panel">
                <img src="/assets/img/iot.jpeg" alt=""></img>
                <div className="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 className="uk-margin-remove">IoT</h3>
                   
                </div>
            </div>
        </li>
    </ul>
    
     <Link className="uk-position-center-left uk-position-small uk-hidden-hover" to="#" uk-slidenav-previous="true" uk-slider-item="previous"></Link>
    <Link className="uk-position-center-right uk-position-small uk-hidden-hover" to="#" uk-slidenav-next="true" uk-slider-item="next"></Link>


</div>
        );
    }
}
