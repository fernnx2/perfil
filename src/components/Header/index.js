import React, {Component} from 'react'
import {Link} from 'react-router-dom';
class Header extends Component {

    render() {

        return (
            <div className="uk-section-primary uk-preserve-color">
                <div
                    uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 200">
                    <div className="uk-container uk-container-expand">
                        <nav
                            className="uk-navbar-container"
                            uk-navbar="dropbar: true; dropbar-mode: push">
                            <div className="uk-navbar-left">

                                <ul className="uk-navbar-nav">
                                    <li>
                                        <h3 className="uk-heading-bullet">Web Developer & Web Designer</h3>
                                    </li>

                                </ul>

                            </div>

                            <div className="uk-navbar-right">
                                <ul className="uk-navbar-nav uk-visible@m">
                                    <li className="margin margintop">

                                        <div className="link-container">
                                            <Link to="/" className="link uk-button uk-button-text">Home</Link>
                                        </div>
                                    </li>
                                    <li className="margin margintop">

                                        <div className="link-container">
                                            <Link to="/skills" className="link uk-button uk-button-text">Skills</Link>
                                        </div>
                                    </li>
                                    <li className="margin margintop">
                                        <div className="link-container">
                                            <Link to="/services" className="link uk-button uk-button-text">Services</Link>
                                        </div>

                                    </li>
                                    <li className="margin margintop">
                                        <button
                                            type="button"
                                            className="uk-button uk-button-text"
                                            uk-toggle="target: #modalContact">Contact</button>
                                    </li>

                                </ul>

                                <a
                                    uk-navbar-toggle-icon=""
                                    href="#offcanvas"
                                    uk-toggle=""
                                    className="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon">
                                    <svg
                                        width="35"
                                        height="35"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"
                                        data-svg="navbar-toggle-icon">
                                        <rect y="9" width="20" height="3"></rect>
                                        <rect y="3" width="20" height="3"></rect>
                                        <rect y="15" width="20" height="3"></rect>
                                    </svg>
                                </a>
                            </div>
                        </nav>
                    </div>
                </div>

                <div
                    id="offcanvas"
                    uk-offcanvas="mode: push; overlay: true"
                    className="uk-offcanvas">
                    <div className="uk-offcanvas-bar">
                        <div className="uk-panel">
                            <ul className="uk-nav uk-nav-default tm-nav">
                                <li className="uk-nav-header">General</li>
                                <li>
                                    <Link to="/">Home</Link>
                                </li>
                                <li>
                                    <Link to="/skills">Skills</Link>
                                </li>
                                <li>
                                    <Link to="/services">Service</Link>
                                </li>
                                <li>
                                    <button
                                        type="button"
                                        className="uk-button uk-button-text"
                                        uk-toggle="target: #modalContact">Contact</button>
                                </li>
                                <li className="uk-nav-divider"></li>
                                <li>
                                    <Link target="_blank" to="/assets/resources/CVfernando.pdf">Download CV</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Header