import React, {Component} from 'react'
import {Link} from 'react-router-dom'
export default class Fotter extends Component {
    render() {
        return (

            <div className="uk-section-secondary">
                <div className="uk-container">
                    <div className="uk-margin-top uk-margin-bottom">
                        <div uk-grid="true" className="uk-child-width-auto@m uk-flex-middle uk-grid">
                            <div className="uk-first-column">
                                <div className="uk-text-left@m uk-text-center">
                                    <a href="./index" className="uk-logo">

                                        <img src="/assets/img/copyright.png" alt="copyright"/>
                                        copyright
                                    </a>

                                </div>
                            </div>
                            <div className="uk-margin-auto">
                                <ul uk-margin="" className="uk-subnav uk-flex-center">
                                    <li className="uk-first-column">
                                        <Link to="#" uk-toggle="#modalAbout">About</Link>
                                    </li>
                                    <li>
                                        <Link to="#" uk-toggle="#modalContact">Contact</Link>
                                    </li>
                                    <li>
                                        <Link to="#" uk-toggle="#modalTC">Terms and Conditions</Link>
                                    </li>

                                </ul>
                            </div>
                            <div>
                                <div className="uk-text-right@m uk-text-center">
                                    <div
                                        uk-grid="true"
                                        className="uk-child-width-auto uk-grid-small uk-flex-center uk-grid"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modalTC" uk-modal="true">
                    <div className="uk-modal-dialog" uk-overflow-auto="true">
                        <button className="uk-modal-close-default" type="button" uk-close="true"></button>
                        <div className="uk-modal-header">
                            <h2 className="uk-modal-title">Term and Conditions</h2>
                        </div>
                        <div className="uk-modal-body">
                            <p>
                                1. The technical skills exposed here are relative to the experience and use in
                                which I have studied and worked, I do not intend at any time to imply that I
                                know the documentation, structure, architecture, or api of each technology
                                mentioned here. However, it is worth mentioning that in a large percentage I
                                have a very good performance in each aforementioned technology.
                            </p>
                            <p>
                                2. Each service, be it development, consultancy, maintenance or any other type
                                of service, is subject to the country where the service is provided, therefore
                                prices are omitted in the different services, to avoid any confusion based on
                                it.
                            </p>
                            <p>
                                3. For the hiring is already temporary, indefinite or otherwise is essential to
                                contact me either by phone, email or other means, to have an agreement directly
                                and effectively.
                            </p>
                            <p>
                                4. The phone number is in El Salvador, you can contact me on my cell phone,
                                whatsapp or email, in case the phone call does not link.
                            </p>
                            <p>
                                5. Payments for services will be stipulated at the time of the contract, these
                                vary according to the locality, generally I use the payment by bank transaction,
                                or paypal. In any case you can define another method of payment as the client
                                wishes.
                            </p>
                            <p>
                                6. Each service has a guarantee of approximately 1 to 3 depending on the type of
                                service. In the same way this will be stipulated in the contracting of the
                                service.
                            </p>

                        </div>

                    </div>
                </div>

                <div id="modalAbout" uk-modal="true">
                    <div className="uk-modal-dialog">
                        <button className="uk-modal-close-default" type="button" uk-close="true"></button>
                        <div className="uk-modal-header">
                            <h2 className="uk-modal-title">About</h2>
                        </div>
                        <div className="uk-modal-body">
                            <img src="/assets/img/perfil.jpg" alt="foto"></img>
                        </div>
                        <p>
                            Website dedicated to my professional profile in the technological and
                            engineering field, made 100% by my person Rene Fernando Menjivar Aguilera
                        </p>

                    </div>
                </div>
            </div>

        );
    }
}