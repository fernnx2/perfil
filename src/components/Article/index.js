import React, {Component} from 'react'
import {Link} from 'react-router-dom'
export default class Article extends Component {
  
    render() {
        return (
            <div
                uk-grid="true"
                className="uk-grid-collapse uk-child-width-expand@s uk-text-center">
              
                    <div className="uk-background-muted uk-padding ">
                        <article className="uk-article">
                            <h1 className="uk-article-title">
                                <Link className="uk-link-reset" to="#">{this.props.title}</Link>
                            </h1>
                            <p className="uk-text-lead">{this.props.text}</p>
                            <p>{this.props.description}</p>

                        </article>
                    </div>
                    <div>
                        <div className="uk-background-primary uk-padding uk-light">
                        <img src={this.props.image} alt="img"></img>
                        </div>
                    </div>

              
            </div>

        )
    }
}