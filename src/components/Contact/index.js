import React, {Component} from 'react'

export default class Contact extends Component {
   
    render() {
        return (
            <div id="modalContact" uk-modal="true" className="">
                <div className="uk-modal-dialog uk-width-large">
                    <button className="uk-modal-close-default" type="button" uk-close="true"></button>

                    {this.props.children}

                </div>
            </div>

        );
    }

}