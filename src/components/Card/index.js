import React, { Component } from 'react'

class Card extends Component {
  
    render() { 
        return (
            <div className="uk-child-width-1-1">
                <div className="uk-card uk-card-small uk-card-hover uk-card-primary">
                    <div className="uk-card-media-top">
                        <img src="/assets/img/perfil.jpg" alt="perfil" />
                    </div>
                    <div className="uk-card-body">
                        <h3 className="uk-card-title">{this.props.name}</h3>
                        <p><span uk-icon="icon: mail; ratio:1" className="uk-margin-small-right uk-icon"></span>{this.props.email}</p>
                        <p> <span uk-icon="icon: whatsapp; ratio:1" className="uk-margin-small-right uk-icon"></span>{this.props.cell}</p>
                        <p> <span uk-icon="icon: receiver; ratio:1" className="uk-margin-small-right uk-icon"></span>{this.props.telephone}</p>
                        <p> <span uk-icon={this.props.state} className="uk-margin-small-right uk-icon"> </span>FreeLancer </p>
                        <p><span uk-icon="location" className="uk-margin-small-right uk-icon"> </span>{this.props.location}</p>
                    </div>
               
                </div>
            </div>
            
        )
    }
}

export default Card