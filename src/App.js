import React, {Component} from 'react'
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import Dashboard from './containers/Dashboard'
import Services from './containers/Services'
import Skills from './containers/Skills'

class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Dashboard}/>
                        <Route  path="/skills" component={Skills}/>
                        <Route  path="/services" component={Services}/>

                    </Switch>
                </BrowserRouter>

            </div>

        )
    }
}

export default App;
