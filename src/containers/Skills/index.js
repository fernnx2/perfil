import React, {Component} from 'react'


import CardSkills from '../../components/CardSkills'
import Skill from '../../components/Skill'
import Contact from '../../components/Contact'
import Card from '../../components/Card'
import Footer from '../../components/Footer'
import Header from '../../components/Header'

class Skills extends Component {

    render() {
        return (
            <div>
                
                <Header></Header>
                <div className="uk-section">
                    <div className="uk-container">

                        <CardSkills title="Front-End">
                            <Skill title="HTML" porcentaje="90"></Skill>
                            <Skill title="CSS" porcentaje="75"></Skill>
                            <Skill title="JavaScript" porcentaje="75"></Skill>
                            <Skill title="React Js" porcentaje="75"></Skill>
                            <Skill title="Vue Js" porcentaje="75"></Skill>
                            <Skill title="Angular Js" porcentaje="75"></Skill>
                            <Skill title="Angular" porcentaje="45"></Skill>
                            <Skill title="JQuery" porcentaje="90"></Skill>
                            <Skill title="Materialize.css" porcentaje="90"></Skill>
                            <Skill title="ED-grid" porcentaje="95"></Skill>
                            <Skill title="Bootstrap.css" porcentaje="75"></Skill>
                            <Skill title="Semantic UI.css" porcentaje="75"></Skill>
                            <Skill title="Material lite .css " porcentaje="60"></Skill>
                            <Skill title="UIkit" porcentaje="75"></Skill>
                        </CardSkills>

                        <CardSkills title="Back-End">
                            <Skill title="Java ES, EE" porcentaje="75"></Skill>
                            <Skill title="PHP5 +7.2" porcentaje="75"></Skill>
                            <Skill title="Node Js" porcentaje="60"></Skill>
                            <Skill title="C#" porcentaje="60"></Skill>
                            <Skill title=".NET" porcentaje="60"></Skill>
                        </CardSkills>

                        <CardSkills title="DataBases">
                            <Skill title="MySQL 5+" porcentaje="90"></Skill>
                            <Skill title="PostgreSQL" porcentaje="75"></Skill>
                            <Skill title="Oracle DataBase" porcentaje="60"></Skill>
                            <Skill title="PLSQL" porcentaje="60"></Skill>
                            <Skill title="SQLite" porcentaje="75"></Skill>
                        </CardSkills>

                        <CardSkills title="Fameworks">
                        <Skill title="Spring MVC" porcentaje="75"></Skill>
                        <Skill title="PrimeFaces" porcentaje="45"></Skill>
                        <Skill title="ORM: JPA, eclipselink,hibernate, eloquent" porcentaje="90"></Skill>
                        <Skill title="Laravel 5+" porcentaje="75"></Skill>
                        <Skill title="JUnit" porcentaje="75"></Skill>
                        <Skill title="Mockito" porcentaje="75"></Skill>
                        </CardSkills>

                        <CardSkills title="Tools">
                            <Skill title="Git,github,gitlab,bitbucket" porcentaje="90"></Skill>
                            <Skill title="Jenkins" porcentaje="60"></Skill>
                            <Skill title="SonarQube" porcentaje="45"></Skill>
                            <Skill title="Mysql Workbench" porcentaje="75"></Skill>
                            <Skill title="Oracle Developer" porcentaje="75"></Skill>
                            <Skill title="UML" porcentaje="75"></Skill>
                            <Skill title="Netbeans 8+" porcentaje="90"></Skill>
                            <Skill title="Eclipse" porcentaje="75"></Skill>
                            <Skill title="vscode" porcentaje="90"></Skill>
                            <Skill title="Maven" porcentaje="75"></Skill>
                            <Skill title="Composer" porcentaje="75"></Skill>
                        </CardSkills>

                        <CardSkills title="Other">
                        <Skill title="Linux" porcentaje="75"></Skill>
                        <Skill title="windows" porcentaje="90"></Skill>
                        <Skill title="IIS" porcentaje="60"></Skill>
                        <Skill title="Apache Server" porcentaje="75"></Skill>
                        <Skill title="Payara Server" porcentaje="75"></Skill>
                        <Skill title="Glassfish, Tomcat" porcentaje="60"></Skill>
                        <Skill title="Docker, Compose, Swarm" porcentaje="75"></Skill>
                        <Skill title="Amazon AWS" porcentaje="60"></Skill>
                        <Skill title="Firebase" porcentaje="75"></Skill>
                        <Skill title="IBM" porcentaje="45"></Skill>
                        <Skill title="Hosting" porcentaje="75"></Skill>
                        <Skill title="Android" porcentaje="60"></Skill>
                        <Skill title="Electronica, IoT" porcentaje="75"></Skill>
                        </CardSkills>

                    </div>
                </div>
                <Contact title="Contact">
                    <Card name="Fernando Menjivar" 
                          email="devfernando95@gmail.com"
                          cell="(503) 76194652"
                          telephone="24084846"
                          state="check"
                          location="El Salvador"></Card>
                </Contact>
               
               <Footer></Footer>
            </div>
        );
    }

}

export default Skills;