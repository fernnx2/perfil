import React, {Component} from 'react'
import Contact from '../../components/Contact';
import Card from '../../components/Card'
import Article from '../../components/Article'
import Header from '../../components/Header';
import Footer from '../../components/Footer'
export default class Service extends Component {

    render() {
        return (
            <div>
                
                <Header></Header>
                <div className="uk-section">
                    <div className="uk-container">

                        <Article
                            title="Web Pages"
                            text="
                    Web pages for all types of public in general, with the best designs."
                            description="Methodology, fast and effective"
                            image="/assets/img/web.jpg"></Article>

                        <Article
                            title="Front-End"
                            text="Web applications for web services consumption, managing business logic, social media, transactions and more."
                            description="development methodology with the new front-end technologies, SPA applications based on web components."
                            image="/assets/img/frontend.jpg"></Article>
                        <Article
                            title="Back-End"
                            text="Apis based on restfull services, for all types of web apps, and commercial applications of different categories."
                            description="Development with robust and effective technologies"
                            image="/assets/img/back.jpg"></Article>
                        <Article
                            title="Microservices"
                            text="Systems with architectures based on microservices. Data analysis, quick-drop containers etc."
                            description="Agile development methodology, unit tests and CI / CD"
                            image="/assets/img/microservicios.png"></Article>
                         <Article
                            title="Business Software"
                            text="Analysis of all types of systems, use cases, requirements survey, business software. IoT, ERP, SAP."
                            description="Desarrollo fullstack ,IoT and legacy software."
                            image="/assets/img/bi.jpg"></Article>

                    </div>
                </div>
                <Contact title="Contact">
                    <Card
                        name="Fernando Menjivar"
                        email="devfernando95@gmail.com"
                        cell="(503) 76194652"
                        telephone="24084846"
                        state="check"
                        location="El Salvador"></Card>
                </Contact>
                <Footer></Footer>
            </div>
        );
    }

}