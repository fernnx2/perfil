import React, {Component} from 'react'
import {Link} from 'react-router-dom';
import "./Dashboard.css";
import Slider from '../../components/Slider';
import Contact from '../../components/Contact';
import Card from '../../components/Card';

export default class Dashboard extends Component {

    render() {

        return (
            <div>
                <div className="uk-section-primary uk-preserve-color">
                    <div
                        uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 200">
                        <div className="uk-container uk-container-expand">
                            <nav
                                className="uk-navbar-container"
                                uk-navbar="dropbar: true; dropbar-mode: push">
                                <div className="uk-navbar-left">

                                    <ul className="uk-navbar-nav">
                                        <li>
                                            <h3 className="uk-heading-bullet">Web Developer & Web Designer</h3>
                                        </li>

                                    </ul>

                                </div>

                                <div className="uk-navbar-right">
                                    <ul className="uk-navbar-nav uk-visible@m">
                                        <li className="margin">

                                            <div className="link-container">
                                                <Link to="/skills" className="link uk-button uk-button-text">Skills</Link>
                                            </div>
                                        </li>
                                        <li className="margin">
                                            <div className="link-container">
                                                <Link to="/services" className="link uk-button uk-button-text">Services</Link>
                                            </div>

                                        </li>
                                        <li className="margin">
                                            <button
                                                type="button"
                                                className="uk-button uk-button-text"
                                                uk-toggle="target: #modalContact">Contact</button>
                                        </li>

                                    </ul>
                                    <div className="uk-navbar-item uk-visible@m">
                                        <a
                                            href="/assets/resources/CVfernando.pdf"
                                            target="_blank"
                                            className="uk-button uk-button-default uk-icon">CV
                                            <canvas uk-icon="icon: download" width="20" height="20" className="uk-icon"/>
                                        </a>
                                    </div>
                                    <a
                                        uk-navbar-toggle-icon=""
                                        href="#navmenu"
                                        uk-toggle=""
                                        className="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon">
                                        <svg
                                            width="35"
                                            height="35"
                                            viewBox="0 0 20 20"
                                            xmlns="http://www.w3.org/2000/svg"
                                            data-svg="navbar-toggle-icon">
                                            <rect y="9" width="20" height="3"></rect>
                                            <rect y="3" width="20" height="3"></rect>
                                            <rect y="15" width="20" height="3"></rect>
                                        </svg>
                                    </a>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div className="uk-section  uk-light uk-width-expand">

                        <div className="uk-container margintop">

                            <Slider></Slider>

                            <p className="uk-margin-medium uk-text-lead uk-hidden@m">

                                Hi, I'm Fernando. I like the technology, the challenges, learn every day more. I
                                like to be responsible and I have a lot of dedi cation in what I do.
                            </p>
                        </div>

                        <div className="uk-container uk-container-xsmall margintop">

                            <div className=" uk-child-width-1-2  uk-text-center " uk-grid="true">
                                <div>

                                    <a
                                        href="https://gitlab.com/fernnx2"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="uk-button uk-button-primary tm-button-primary uk-button-large tm-button-large uk-visible@s">
                                        <img src="https://img.icons8.com/ios/25/000000/gitlab-filled.png" alt="x"></img>
                                        Gitlab
                                    </a>
                                    <a
                                        href="https://gitlab.com/fernnx2"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="uk-button uk-button-primary tm-button-primary uk-hidden@s">Gitlab</a>

                                </div>

                                <div>
                                    <a
                                        href="https://github.com/fernnx2"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="uk-button uk-button-default tm-button-default uk-button-large tm-button-large uk-visible@s">
                                        <span uk-icon="icon: github; ratio: 1"></span>
                                        Github</a>
                                    <a
                                        href="https://github.com/fernnx2"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="uk-button uk-button-default tm-button-default uk-hidden@s">
                                        <span uk-icon="icon: github; ratio: 1"></span>
                                        Github</a>
                                </div>
                            </div>
                        </div>

                        <div className="uk-container uk-container-small uk-text-center">
                            <div
                                className="uk-container uk-container-expand uk-text-center uk-position-relative">
                                <ul
                                    uk-margin=""
                                    className="uk-subnav tm-subnav uk-flex-inline uk-flex-center uk-margin-remove-bottom">
                                    <li className="uk-first-column">
                                        <span>Version
                                            <span uikit-version="">1</span>
                                        </span>
                                    </li>
                                    <li>
                                        <a href="https://youpost.cf" rel="noopener noreferrer" target="_blank">
                                            <span uk-icon="icon: star" className="uk-margin-small-right uk-icon">
                                                <svg
                                                    width="20"
                                                    height="20"
                                                    viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    data-svg="star">
                                                    <polygon
                                                        fill="none"
                                                        stroke="#000"
                                                        points="10 2 12.63 7.27 18.5 8.12 14.25 12.22 15.25 18 10 15.27 4.75 18 5.75 12.22 1.5 8.12 7.37 7.27"></polygon>
                                                </svg>
                                            </span>Blog</a>
                                    </li>
                                    <li>
                                        <a
                                            href="https://www.linkedin.com/in/fernando-menjivar-b806b7155/"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            uk-icon="icon: linkedin">
                                        </a>
                                    </li>
                                    <li>
                                        <Link to="#">
                                            <span uk-icon="icon: commenting" className="uk-margin-small-right uk-icon">
                                                <svg
                                                    width="20"
                                                    height="20"
                                                    viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    data-svg="commenting">
                                                    <polygon
                                                        fill="none"
                                                        stroke="#000"
                                                        points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                                                    <circle cx="10" cy="8" r="1"></circle>
                                                    <circle cx="6" cy="8" r="1"></circle>
                                                    <circle cx="14" cy="8" r="1"></circle>
                                                </svg>
                                            </span>Community</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div id="navmenu" uk-offcanvas="true">
                        <div className="uk-offcanvas-bar">

                            <ul className="uk-nav uk-nav-default">

                                <li>
                                    <h5>Fernando</h5>
                                </li>
                                <li className="uk-nav-header">Actions</li>
                                <li>
                                    <Link to="/skills">Skills</Link>
                                </li>
                                <li>
                                    <Link to="/services">Services</Link>
                                </li>
                                <li>
                                    <button
                                        type="button"
                                        className="uk-button uk-button-text"
                                        uk-toggle="target: #modalContact">Contact</button>
                                </li>
                                <li className="uk-nav-divider"></li>
                                <li>
                                    <a target="_blank" href="/assets/resources/CVfernando.pdf">Download CV</a>
                                </li>
                            </ul>

                        </div>

                    </div>

                </div>
                <Contact title="Contact">
                    <Card
                        name="Fernando Menjivar"
                        email="devfernando95@gmail.com"
                        cell="(503) 76194652"
                        telephone="24084846"
                        state="check"
                        location="El Salvador"></Card>
                </Contact>

            </div>
        )

    }

}